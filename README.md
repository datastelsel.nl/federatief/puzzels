# puzzels

Dit zijn 'de' puzzels voor een [federatief
datastelsel](https://federatief.datastelsel.nl). Deze zijn vast niet compleet en
volledig en/of volledig duidelijk. Aanvulling, verbeteringen, toevoegingen zijn
zeer welkom!

Deze puzzels zijn gepubliceerd op:

- [federatief.datastelsel.nl/puzzels](https://federatief.datastelsel.nl) ( :warning: under construction)
- [digilab.overheid.nl/puzzels](https://digilab.overheid.nl/puzzels)

## Contributing

Contributions are most welcome. See
[federatief.datastelsel.nl/contributie](https://federatief.datastelsel.nl/docs/contribution/)
for more instructions.

## Authors and acknowledgment

This project is the collaboration of
[federatief.datastelsel.nl](https://federatief.datastelsel.nl),
[Digilab](https://digilab.overheid.nl) supported by
[MinBZK](https://github.com/minbzk/) and [Realisatie
IBDS](https://realisatieibds.nl).

## License

This project is licensed under the [EUPL-1.2](./LICENSE.md).
