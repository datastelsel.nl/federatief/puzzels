---
title: Enkelvoudige bevragingen
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T14:18:06.928Z
description: Wat is de best practice?
---
Het gaat hier om de standaardisatie, want het kan al op talloze manieren, maar een uniforme wijze is wenselijk. Gaat bijvoorbeeld ook over hoe met expands / inclusions, etc. wordt omgegaan; wat is de best practice?
