---
title: Policies
draft: false
puzzel-categorie: Vertrouwen > Toegang
date: 2023-11-22T10:27:49.958Z
description: Hoe kunnen we policies standaardiseren, zodat deze uitwisselbaar en
  bewijsbaar worden?
---
Hoe kunnen we policies standaardiseren, zodat deze uitwisselbaar en bewijsbaar worden?  Denk hierbij aan XACML en Open Policy Agent.  Toepasbaar voor policies op allerlei onderwerpen, zoals infrastructuur, autorisatie op connecties, autorisatie bij databevragingen en het gebruik van functies binnen applicaties.
