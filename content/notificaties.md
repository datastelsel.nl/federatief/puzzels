---
title: Notificaties
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T14:37:57.853Z
description: Bedoeling is om tot een standaard te komen om notificaties te
  sturen na een (reeks van) event(s).
---
De bedoeling is om tot een standaard te komen om notificaties te sturen na een (reeks van) event(s). Binnen die standaard moeten dan aspecten als abonneren, privacy by design aspecten, etc. worden opgenomen.
