---
title: Translatie
draft: false
puzzel-categorie: Interoperabiliteit > Modellen
date: 2023-11-22T12:55:51.206Z
description: Op die manier weten aanbieders en bevragers van data in het
  federatieve datastelsel in welke vormen ze data kunnen vragen of leveren.
---
Voor de juiste werking van een federatief datastelsel zijn standaarden nodig op het gebied van translatie: de omzetting van een gegeven naar een andere vorm of een ander formaat. Op die manier weten aanbieders en bevragers van data in het stelsel in welke vormen ze data kunnen vragen of leveren.
