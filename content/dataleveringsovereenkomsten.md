---
title: Dataleveringsovereenkomsten
draft: false
puzzel-categorie: Governance > Operationeel
date: 2024-02-06T15:21:26.406Z
description: Om grip te houden op de gegevenshuishouding is het belangrijk om
  over levering en gebruik duidelijke afspraken te maken tussen
  (gegevens)leverancier en afnemer. De vastlegging van deze afspraken kan in de
  vorm van een Gegevens Levering Overeenkomst (GLO).
---
Om grip te houden op de gegevenshuishouding is het belangrijk om over levering en gebruik duidelijke afspraken te maken tussen (gegevens)leverancier en afnemer. De vastlegging van deze afspraken kan in de vorm van een Gegevens Levering Overeenkomst (GLO). De GLO is bedoeld voor reguliere leveringen, en niet voor eenmalige bevragingen of informatieaanvragen. Het is van belang dat leverancier en afnemer van elkaar weten welke kwaliteit van gegevens geleverd kan worden. Voorbeelden van vragen die daarbij beantwoord horen te zijn: hoe actueel zijn de gegevens, wat is de vullingsgraad van het gegeven en wat is afgesproken over geconstateerde afwijkingen (bijvoorbeeld terugmeldingsplicht)?
De AVG eist van zowel verwerkingsverantwoordelijken als verwerkers dat er een verwerkersovereenkomst is (artikel 28, lid 3 van de Algemene verordening gegevensbescherming, de AVG). Beide partijen zijn dus aansprakelijk als zo’n overeenkomst ontbreekt.
Voor het FDS is het goed om te komen tot een standaardisatie van deze overeenkomsten (datalevering, verwerkingen) tussen partijen die data van elkaar gebruiken binnen het FDS.
