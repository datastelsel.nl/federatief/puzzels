---
title: Automatisering beheer koppelingen
draft: false
puzzel-categorie: Vertrouwen > Veiligheid
date: 2023-11-22T10:31:13.643Z
description: Om koppelingen tussen organisaties/applicaties makkelijk en
  probleemloos schaalbaar te maken is het nodig om ze te automatiseren.
---
Om koppelingen tussen organisaties/applicaties makkelijk en probleemloos schaalbaar te maken is het nodig om ze te automatiseren. Er zijn al bestaande methodes om dit te regelen (zoals FSC), maar zijn er ook alternatieven?
