---
title: Orchestratie
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T14:48:15.743Z
description: Orchestratie gaat over het samenstellen van een antwoord uit
  meerdere requests / bronnen. De technische uitdaging is hier hoe orchestratie
  (aspect van integratie) te doen in een FDS en deze te standaardiseren.
---
Orchestratie gaat over het samenstellen van een antwoord uit meerdere requests / bronnen. De technische uitdaging is hier hoe orchestratie (aspect van integratie) te doen in een FDS en deze te standaardiseren.
