---
title: Homomorfe encryptie
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T15:01:57.881Z
description: Homomorfe encryptie is een vorm van encryptie die het mogelijk
  maakt berekeningen te maken op versleutelde tekst.
---
Homomorfe encryptie is een vorm van encryptie die het mogelijk maakt berekeningen te maken op versleutelde tekst. Het resultaat van deze berekening is ook versleuteld, maar komt na ontsleuteling overeen met het resultaat als dezelfde berekening was uitgevoerd op de oorspronkelijke klare tekst. Nu wordt bij elke use case waarbij dit nuttig is, het wiel vaak opnieuw uitgevonden. Dit kan worden voorkomen door een standaardisatie van toepassing homomorfe encryptie te realiseren.
