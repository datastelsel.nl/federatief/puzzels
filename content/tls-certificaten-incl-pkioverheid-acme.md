---
title: TLS-certificaten (incl. PKIOverheid) - ACME
draft: false
puzzel-categorie: Vertrouwen > Identiteit
date: 2023-11-22T13:54:11.909Z
description: De automatisering van het beheer van beveligings-certificaten
  (aanvraag, uitgifte, verlenging en beëindiging) zorgt ervoor dat dit proces
  makkelijk schaalbaar en minder foutgevoelig wordt.
---
Transport Layer Security certificaten (TLS-certificaten), ook wel bekend als SSL of digitale certificaten, zijn de basis van beveiliging op internet. TLS/SSL-certificaten beveiligen internetverbindingen door de gegevens te versleutelen die worden verzonden. De aanvraag en afspraken tot installatie van dergelijke certificaten, waaronder deze voor overheden (PKIOverheid-certificaten) gaat nog grotendeels handmatig. Dit terwijl het verlengen/vernieuwen van deze certificaten, door steeds snellere ontwikkelingen en ook voor goede beveiliging steeds vaker moeten worden aangevraagd en geïnstalleerd.  De automatisering hiervan, bijv. middels ACME, zou veel efficiënter en goedkoper zijn.
