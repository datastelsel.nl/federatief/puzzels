---
title: Classificatie
draft: false
puzzel-categorie: Datawaarde > Metadata
date: 2023-11-22T16:30:32.247Z
description: Uitdaging hierin is om te komen tot standaardisatie en automatische
  doorwerking van classificaties vanuit de BIO/NIS2.
---
Uitdaging hierin is om te komen tot standaardisatie en automatische doorwerking van classificaties vanuit de [BIO](puzzels/baseline-informatieveiligheid-overheid/)/NIS2. De dataclassificatie laat zien welke informatiesystemen/applicaties en gegevens een hoger niveau van bescherming nodig hebben, dan andere. Ook blijkt daaruit welke met een lager beschermingsniveau toch voldoende zijn beschermd. De classificatie dient de drie aspecten van informatiebeveiliging: Beschikbaarheid, Integriteit en Vertrouwelijkheid.

De Baseline Informatiebeveiliging Overheid (BIO) is het gezamenlijk normenkader voor informatiebeveiliging binnen de gehele overheid, gebaseerd op de internationaal erkende en actuele ISO-normatiek, deze is in 2022 geëvalueerd.

NIS2 Directive(Network and Information Systems) is een Europese richtlijn om de cybersecurity in heel Europa te verbeteren. Als gevolg van de evaluatie van de BIO en de komst van de NIB2/NIS2 wordt een nieuwe BIO verwacht in 2024. Vooruitlopend op de nieuwe BIO is alvast de handreiking BIO 2.0-opmaat gepubliceerd. Hiermee kunnen overheidsmaatregelen gebruikt worden in de context en indeling van NEN-EN-ISO/IEC 27002:2022 (nl), in lijn met de huidige dreigingen.
