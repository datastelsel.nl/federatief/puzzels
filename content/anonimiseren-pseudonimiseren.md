---
title: Anonimiseren/pseudonimiseren
draft: false
puzzel-categorie: Vertrouwen > Veiligheid
date: 2023-11-22T15:05:21.811Z
description: "Standaardisatie van anonimisering en pseudonimisering: Wanneer pas
  je welke optie toe, en hoe doen we dat dan?"
---
Encryptie (versleuteling) en pseudonimisering komen op hetzelfde neer. Met de juiste sleutel of aanvullende gegevens kunnen de (persoons)gegevens ontsleuteld en leesbaar gemaakt worden, waardoor herleiden tot individuen weer mogelijk is. Anonimisering is daarentegen onomkeerbaar, waarbij na toepassing ervan herleiden van gegevens tot individuen niet meer mogelijk is. Vormen hiervan zijn randomisatie en generalisatie. Wanneer sprake is van volledig geanonimiseerde data, is privacywetgeving niet meer van toepassing, maar op gepseudonimiseerde data wel. Standaardisatie van anonimisering en pseudonimisering: Wanneer pas je welke optie toe, en hoe doen we dat dan?
