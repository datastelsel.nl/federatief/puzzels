---
title: Hardening
draft: false
puzzel-categorie: Vertrouwen > Veiligheid
date: 2023-11-22T16:15:29.436Z
description: Best practices voor hardening, het minimaliseren van risico tot
  hacks. Het gaat hier om het toepassen van zero trust (authenticatie, bijv.
  2-factor) en policies hiervoor.
---
Best practices voor hardening, het minimaliseren van risico tot hacks. Het gaat hier om het toepassen van zero trust (authenticatie, bijv. 2-factor) en policies hiervoor.
