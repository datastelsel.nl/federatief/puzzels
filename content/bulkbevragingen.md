---
title: Bulkbevragingen
draft: false
puzzel-categorie: Interoperabiliteit > Dataservices
date: 2023-11-22T14:19:00.120Z
description: Bulkbevragingen worden typisch gebruikt in geval van analyse,
  daarvoor zijn soortgelijke mechanismes nodig, dus standaarden, qua logging /
  vastleggen grondslag.
---
Bulkbevragingen worden typisch gebruikt in geval van analyse, daarvoor zijn soortgelijke mechanismes nodig, dus standaarden, qua logging / vastleggen grondslag. De focus ligt er dan meer op dat er een kopie van een set wordt gemaakt met heldere scope qua doel, zodat dit juridisch is geborgd. Voor iedere analyse wordt opnieuw een set opgevraagd. Er dient te worden geborgd dat dat elke keer op dezelfde manier wordt gedaan.
