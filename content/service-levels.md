---
title: Service levels
draft: false
puzzel-categorie: Governance > Operationeel
date: 2024-02-06T15:35:24.313Z
description: >
  Standaardiseren van afspraken over service levels (indien nodig) voor
  bijvoorbeeld becschikbaarheid infra, support, en API performance. 
---
Standaardiseren van afspraken over service levels (indien nodig) voor bijvoorbeeld becschikbaarheid infra, support, en API performance. 
Met de vastlegging (en standaardisering) van afspraken over service levels kan worden geborgd, dat een bepaald niveau van dienstverlening door de verschillende partijen geleverd wordt. Tevens kan het de basis vormen voor een Service Level Rapportage, waarmee bewaakt kan worden dat de afspraken / levels worden gehaald.
