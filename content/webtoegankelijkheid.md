---
title: Webtoegankelijkheid
draft: false
puzzel-categorie: Governance > Operationeel
date: 2024-02-06T15:38:04.675Z
description: Ondersteunen bij compliancy op gebied toegankelijkheid door onder
  andere validators en best practices rond Continuous Integration.
---
Ondersteunen bij compliancy op gebied toegankelijkheid door onder andere validators en best practices rond Continuous Integration.
