---
title: Authenticatie (organisaties, personen)
draft: false
puzzel-categorie: Vertrouwen > Identiteit
date: 2023-11-22T16:26:12.298Z
description: Standaardiseren van authenticatie, per patroon. Voor een patroon
  Business to Government (B2G) kunnen andere standaarden nodig zijn dan voor een
  patroon Government to Government (G2G). Het streven is om te komen tot een
  interoperabele oplossing voor de diverse standaarden die hier nu voor bestaan.
---
Standaardiseren van authenticatie, per patroon. Voor een patroon Business to Government (B2G) kunnen andere standaarden nodig zijn dan voor een patroon Government to Government (G2G). Het streven is om te komen tot een interoperabele oplossing voor de diverse standaarden die hier nu voor bestaan.
