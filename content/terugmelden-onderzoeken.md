---
title: Terugmelden, onderzoeken
draft: false
puzzel-categorie: Governance > Operationeel
date: 2023-11-22T16:32:38.629Z
description: Een adequate functie voor terugmelden van vermeende onjuistheden
  in databronnen is van hoog belang om kwaliteit van stelseldata te kunnen
  waarborgen.
---
Voor het Federatief Datastelsel is kwaliteit een belangrijk criterium bij aansluiting van data-aanbieders en databronnen. Een adequate functie voor terugmelden van vermeende onjuistheden in databronnen is van hoog belang om kwaliteit van stelseldata te kunnen waarborgen. Ook bij deze functie zal het in beginsel gaan om een standaard en niet om een centrale voorziening.

Zie ook: <https://www.noraonline.nl/wiki/FDS_Technische_functies>
