---
title: Correcte delegatie van rechten
draft: false
puzzel-categorie: Vertrouwen > Identiteit
date: 2024-02-06T12:38:48.822Z
description: Delegatie van toegang tot data van een andere organisatie op andere
  niveaus dan de eigen organisatie. Er zijn diverse zienswijzen op. Door
  zienswijzes in modellen uit te werken kan daarmee de keuze voor te hanteren
  zienswijze worden gevoed.
---
Delegatie van toegang tot data van een andere organisatie op andere niveaus dan de eigen organisatie. Er zijn diverse zienswijzen op. Door zienswijzes in modellen uit te werken kan daarmee de keuze voor te hanteren zienswijze worden gevoed.
