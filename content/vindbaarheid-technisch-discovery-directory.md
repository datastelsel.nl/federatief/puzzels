---
title: Vindbaarheid (technisch, discovery), directory
draft: false
puzzel-categorie: Datawaarde > Metadata
date: 2023-11-22T16:27:02.533Z
description: Het gaat hier om de technische tegenhanger van een catalogus. Waar
  een catalogus informatie geeft over inhoud, geeft een directory verwijzingen
  over hoe je iets technisch gezien kunt vinden en hoe je kunt koppelen -
  betreft dus de routering.
---
Het gaat hier om de technische tegenhanger van een catalogus. Waar een catalogus informatie geeft over inhoud, geeft een directory verwijzingen over hoe je iets technisch gezien kunt vinden en hoe je kunt koppelen - betreft dus de routering. Een standaardisatie van een service discovery is wenselijk binnen een stelsel. Er wordt bijvoorbeeld gekeken naar de FSC Directory (Federated Service Connectivity (FSC) standaard): <https://commonground.gitlab.io/standards/fsc>
