---
title: Schaalbaarheid
draft: false
puzzel-categorie: Governance > Beheer
date: 2023-11-22T11:04:41.340Z
description: Bij het onderzoeken van oplossingen voor het efficiënt horizontaal
  schalen van infrastructuur is het van belang om de keuze hiervoor al in de
  ontwerpfase vast te leggen.
---
Bij het onderzoeken van oplossingen voor het efficiënt horizontaal schalen van infrastructuur is het van belang om de keuze hiervoor al in de ontwerpfase vast te leggen. Welke afspraken zijn hiervoor nodig? Welke best practices zijn er?
