---
title: Duurzame toegankelijkheid
draft: false
puzzel-categorie: Governance > Beheer
date: 2023-11-22T11:50:39.237Z
description: Hoe ondersteun je organisaties op het gebied van compliancy, met
  validators en best practices rond continuous integration?
---
Informatie is duurzaam toegankelijk als die gedurende de vastgestelde bewaartermijn voldoet aan de kenmerken op het gebied van Duurzame Toegankelijkheid: vindbaar, beschikbaar, leesbaar, interpreteerbaar, betrouwbaar en toekomstbestendig. Voor iedereen die daar recht op heeft en voor zo lang als noodzakelijk. Hoe ondersteun je organisaties op het gebied van compliancy, met validators en best practices rond continuous integration?
