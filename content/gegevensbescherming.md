---
title: Gegevensbescherming
draft: false
puzzel-categorie: Governance > Operationeel
date: 2024-02-06T15:29:23.359Z
description: Hier is het doel om op basis van best practices, standaardisering
  van configuraties en juridische toetsing invulling te geven aan nodig is op
  infrastructuurniveau om compliant te zijn.
---
Hier is het doel om op basis van best practices, standaardisering van configuraties en juridische toetsing invulling te geven aan nodig is op infrastructuurniveau om compliant te zijn aan de Baseline Informatiebeveiliging Overheid (BIO) voor overheidssystemen in het algemeen en aan de Algemene Verordening gegevensbescherming (AVG) meer specifiek voor privacybescherming. https://www.gemmaonline.nl/index.php/Informatiebeveiliging_en_Privacy_binnen_de_Omgevingswet
