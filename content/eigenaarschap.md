---
title: Eigenaarschap
draft: false
puzzel-categorie: Governance > Bestuurlijk
date: 2023-11-22T11:54:44.232Z
description: Bij roerende en onroerende zaken kun je spreken van eigenaarschap,
  en een eigenaar is verantwoordelijk voor haar of zijn eigendom. Bij data ligt
  dat minder zwart-wit.
---
Bij roerende en onroerende zaken kun je spreken van eigenaarschap, en een eigenaar is verantwoordelijk voor haar of zijn eigendom. Bij data ligt dat minder zwart-wit. Wie is "eigenaar" van data, en wat betekent dat in termen van verantwoordelijkheid? Hoe zit dat met verwerkingen van data, al dan niet met behulp van algoritmen? Op ieder punt van data-uitwisseling kunnen vragen ontstaan over de juridische verantwoordelijkheid: voor de infrastructuur, voor de gegevens zelf, en voor wat er met de gegevens gebeurt. Om verantwoord en veilig data uit te wisselen, is het nodig om hier meer helderheid over te verschaffen in de vormen van afspraken en standaarden.
