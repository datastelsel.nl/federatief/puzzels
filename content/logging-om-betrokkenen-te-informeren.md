---
title: Logging om betrokkenen te informeren
draft: false
puzzel-categorie: Datawaarde > Verantwoording
date: 2023-11-22T13:59:16.083Z
description: Loggegevens kunnen ook inzicht geven aan betrokkenen, om te tonen
  wat er met hun gegevens is gebeurd. Is dit toegestaan, en zo ja, onder welke
  voorwaarden is dit toegestaan?
---
Organisaties gebruiken logging voor allerhande doeleinden: om technische fouten op te kunnen sporen, om analyses te doen, om te kunnen controleren en verantwoorden dat ze zorgvuldig met gegevens omgaan. Iedere organisatie pakt dit op haar eigen manier aan. Loggegevens kunnen ook inzicht geven aan betrokkenen, om te tonen wat er met hun gegevens is gebeurd. Is dit toegestaan, en zo ja, onder welke voorwaarden is dit toegestaan? Eenduidige juridische en technische richtlijnen of standaarden zouden helpen om dit overheidsbreed veilig en verantwoord te regelen.
